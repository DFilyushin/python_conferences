# Create your admin actions here.
from json import loads
from django.views.generic import FormView
from django.contrib import messages
from django.contrib.admin.helpers import ACTION_CHECKBOX_NAME
from django.utils.decorators import classonlymethod
from django.utils.translation import ugettext_lazy as _
from django.forms.models import model_to_dict
from .forms import YesNoForm


class ActionView(FormView):
    """docstring for ActionView."""

    queryset = admin = model = _answer = request = None
    template_name = 'core/admin_confirmation.html'
    allow_empty = False
    short_description = ''
    description = ''
    counter = 0
    chancel_message = _('Action was cancelled')
    finished_message = ''
    processing_message = _('{} objects was change')
    extra_context = {'action_checkbox_name': ACTION_CHECKBOX_NAME}
    permission_error_message = _('Not enough permissions')
    permission = None
    show_loader = True

    @property
    def answer(self):
        self._answer = self._answer or YesNoForm(**super().get_form_kwargs())
        return self._answer

    def post(self, request, *args, **kwargs):
        if self.answer.is_valid():
            if self.answer.is_positiv():
                if self.get_form_class():  # answer is Positiv, processing with or without form
                    return super().post(request, *args, **kwargs)  # form processing
                return self.processing(*args, **kwargs)  # processing without form
            return self.finished(chancel=True)  # answer is Negativ
        self.request.method = 'GET'
        return self.get(request, *args, **kwargs)  # answer is not valid

    def get_queryset(self):
        return self.queryset or self.model and self.model.objects or self.admin and self.admin.get_queryset(self.request)

    def form_valid(self, *args, **kwargs):
        """Make something with cleaned data befor processing."""
        return self.processing(*args, **kwargs)

    def processing(self, *args, **kwargs):
        """Make main process."""
        return self.finished(*args, **kwargs)

    def finished(self, *args, chancel=False, message='', **kwargs):
        """Send message after process and return None."""
        message = chancel and self.chancel_message or message or self.finished_message
        if message:
            self.info(message, *args, counter=self.counter, **kwargs)

    @classonlymethod
    def as_view(cls, **initkwargs):
        response = super(ActionView, cls).as_view(**initkwargs)
        response.short_description = cls.short_description
        response.allow_empty = cls.allow_empty
        return response

    def dispatch(self, admin, request, queryset, *args, **kwargs):  # pylint: disable=arguments-differ
        vars(self).update(admin=admin, model=admin.model, queryset=queryset, request=request)
        return super().dispatch(request, *args, **kwargs)

    def log(self, obj, action='change', change_message=None, **__):
        logger = getattr(self.admin, f'log_{action}', None) or self.admin.log_change
        logger(self.request, obj, change_message or self.processing_message)

    def info(self, info, *args, message_type='info', **kwargs):
        info = info.format(*args, **kwargs)
        getattr(messages, message_type, messages.info)(self.request, info)
        return info

    def error(self, error, **kwargs):
        return self.info(error, message_type='error', **kwargs)

    def warning(self, warning, **kwargs):
        return self.info(warning, message_type='warning', **kwargs)

    def get_form(self, form_class=None):
        return self.get_form_class() and super().get_form(form_class)

    def check_permissions(self, permission, fall_silently=False):  # pylint: disable=inconsistent-return-statements
        if self.model and self.request:
            opts = self.model._meta  # pylint: disable=protected-access
            if self.request.user.has_perm(f'{opts.app_label}.{permission}_{opts.model_name}'):
                return True
            if not fall_silently:
                self.error(self.permission_error_message)

    def get_context_data(self, **kwargs):
        opts = self.model._meta  # pylint: disable=protected-access
        context = {
            'action_name': type(self).__name__,
            'objects_name': opts.verbose_name_plural,
            'objects': self.queryset and len(self.queryset) and self.queryset or (),
            'opts': opts,
            'app_label': opts.app_label,
            'answer': self.answer,
            'title': f'{self.short_description}. {_("Are you sure?")}',
            'action_short_description': self.short_description,
            'action_description': self.description,
        }
        if not self.get_form_class():
            context['form'] = ''

        return super().get_context_data(**(context | kwargs))


class ExportView(ActionView):
    """docstring for ExportView."""
    short_description = _('Export objects')
    permission = 'delete'
    finished_message = '{counter} objects was change'

    def processing(self, *args, **kwargs):
        if self.check_permissions(self.permission):
            for self.counter, obj in enumerate(self.get_queryset(), 1):
                self.info('{}', model_to_dict(obj))
        return super().processing(*args, **kwargs)
