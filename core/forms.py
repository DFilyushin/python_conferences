from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.conf import settings

class YesNoForm(forms.Form):
    """docstring for YesNoForm."""
    BUTTON = '<button type="submit" name="answer" onclick="document.getElementById(`layer`).style.display = `block`;" value="{value}">{title}</button>'.format
    POSITIV = 'yes'
    NEGATIV = 'no'
    CHOICES = {POSITIV: _('Yes, I\'m sure'), NEGATIV: _('No, I\'m not sure')}

    class Media:  # pylint: disable=missing-class-docstring, too-few-public-methods
        css = {'all': ('/static/admin/css/loads.css?v=%s' % settings.VERSION,)}

    answer = forms.ChoiceField(choices=CHOICES.items())

    def is_positiv(self):
        return self.is_valid() and self.cleaned_data['answer'] == self.POSITIV

    def _render_button(self, key):
        return mark_safe(self.BUTTON(value=key, title=self.CHOICES[key]))

    def render_positiv(self):
        return self._render_button(self.POSITIV)

    def render_negativ(self):
        return self._render_button(self.NEGATIV)
