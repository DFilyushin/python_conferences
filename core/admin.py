from admins.admin import default_admin
from .actions import ActionView
from django.template.loader import get_template

from django.contrib.admin import ModelAdmin, TabularInline, StackedInline
from django.conf import settings
from django.contrib.auth.models import Permission
from django.contrib.admin.sites import site

from .models import Product, Image, Store, models, Category, UNKNOWN

site.register(Permission)





class ImageInline(TabularInline):
    model = Image
    extra = 0




class ProductAdmin(ModelAdmin):
    fields = 'name', 'active'
    #readonly_fields = 'inlinefield',
    inlines = ImageInline,

    def inlinefield(self, obj):
        context = obj._context.copy()
        #inline = context['inline_admin_formset'] = context['inline_admin_formsets'].pop(0)
        return ''  #get_template(inline.opts.template).render(context, context.get('request'))

    def render_change_form(self, *args, **kwargs):
        response = super().render_change_form(*args, **kwargs)
        response.context_data['adminform'].form.instance._context = response.context_data
        return response

default_admin.register(Product, ProductAdmin)

class ProxyStore(Store):
    class Meta:
        proxy = True


class ProductProxyAdmin(ModelAdmin):

    def get_queryset(self, request,):
        return super().get_queryset(request).filter(category=self.model.base)


class CategoryAdmin(ModelAdmin):

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if not change:
            Meta = type('Meta', (object,), {'proxy': True, 'verbose_name': obj.name})
            proxy_model_dict = {'Meta': Meta, 'base': obj, '__module__': Product.__module__}
            ProxyProductModel = type(f'attr_{obj.pk}', (Product,), proxy_model_dict)
            default_admin.register(ProxyProductModel, ProductProxyAdmin)

            from django.urls.base import clear_url_caches
            from importlib import reload, import_module
            reload(import_module(settings.ROOT_URLCONF))
            clear_url_caches()

default_admin.register(Category, CategoryAdmin)


class ProductInline(StackedInline):
    model = Product
    fields = 'name', 'inlinefield', 'active'
    readonly_fields = 'inlinefield',
    extra = 1

    def inlinefield(self, obj):
        context = self._context.copy()
        admin_view = type(self.admin_site._registry[self.model])(self.model, self.admin_site).add_view(context.get('_request'))
        inline = context['inline_admin_formset'] = admin_view.context_data['inline_admin_formsets'][0]
        return get_template(inline.opts.template).render(context, context.get('request'))



class StoreAdmin(ModelAdmin):
    fields = 'name', 'active'
    inlines = ProductInline,

    def render_change_form(self, *args, **kwargs):
        response = super().render_change_form(*args, **kwargs)
        response.context_data['_request'] = response._request
        for inline in response.context_data['inline_admin_formsets']:
            inline.opts._context = response.context_data
        return response

default_admin.register(Store, StoreAdmin)


class ProxyStoreAdmin(StoreAdmin):
    inlines = ()

default_admin.register(ProxyStore, ProxyStoreAdmin)
