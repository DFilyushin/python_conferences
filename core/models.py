from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.mixins import ModelMixin

UNKNOWN = _('unknown')

class AbstractModel(ModelMixin, models.Model):
    class Meta:
        abstract = True

    name = models.CharField(_('Name'), max_length=25, blank=True, default='')
    description = models.TextField(_('Description'), blank=True, default='')
    active = models.BooleanField(_('Active'), default=False)
    visible = models.BooleanField(_('Visible'), default=True)
    sorting = models.PositiveSmallIntegerField(_('Sorting'), blank=True, null=True)

    def __str__(self):
        return f'{self.name or self.pk or UNKNOWN}'


class Store(AbstractModel):

    class Meta:
        verbose_name = _('Store')
        verbose_name_plural = _('Stores')


class Category(AbstractModel):

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')


class Product(AbstractModel):

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    category = models.ForeignKey('Category', related_name='products', blank=True, null=True, on_delete=models.SET_NULL)
    parent = models.ForeignKey('Store', related_name='products', blank=True, null=True, on_delete=models.SET_NULL)


class Image(AbstractModel):

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Image')

    parent = models.ForeignKey('Product', related_name='images', on_delete=models.CASCADE)
    src = models.ImageField(verbose_name = 'Image')