from django.urls import reverse, NoReverseMatch
from django.utils.encoding import force_text
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe


class BaseButton(object):
    base_css_classes = ("btn", "")

    def __init__(self, text="", icon=None, disable_reason=None, tooltip=None,
                 extra_css_class="", required_permissions=(), **kwargs):
        """
        :param text: The actual text for the button.
        :param icon: Icon CSS class string
        :param disable_reason: The reason for this button to be disabled.
                               It's considered good UX to have an user-visible reason for disabled
                               actions; thus the only way to disable an action is to set the reason.
                               See http://stackoverflow.com/a/372503/51685.
        :type disable_reason: str|None
        :param tooltip: Tooltip string, if any. May be replaced by the disable reason.
        :type tooltip: str|None
        :param extra_css_class: Extra CSS class(es)
        :type extra_css_class: str
        :param required_permissions: Optional iterable of permission strings
        :type required_permissions: Iterable[str]
        """
        self.text = text
        self.icon = icon
        self.disable_reason = disable_reason
        self.disabled = bool(self.disable_reason)
        self.tooltip = (self.disable_reason or tooltip)
        self.extra_css_class = extra_css_class
        self.required_permissions = required_permissions

    def render(self, request):
        """
        Yield HTML bits for this object.
        :type request: HttpRequest
        :rtype: Iterable[str]
        """
        return ()

    def render_label(self):
        bits = []
        if self.icon:
            bits.append('<i class="%s"></i>&nbsp;' % self.icon)
        bits.append(conditional_escape(self.text))
        return "".join(force_text(bit) for bit in bits)

    def get_computed_class(self):
        return " ".join(filter(None, list(self.base_css_classes) + [
            self.extra_css_class,
            "disabled" if self.disabled else ""
        ]))


class URLButton(BaseButton):
    """
    An action button that renders as a link leading to `url`.
    """

    def __init__(self, url, **kwargs):
        """
        :param url: The URL to navigate to. For convenience, if this contains no slashes,
                    `reverse` is automatically called on it.
        :type url: str
        """
        if "/" not in url:
            try:
                url = reverse(url)
            except NoReverseMatch:
                pass
        self.url = url

        super(URLButton, self).__init__(**kwargs)

    def render(self, request):
        if request.user.has_perms(self.required_permissions):
            return mark_safe(f'<a href="{self.url}" class="{self.get_computed_class()}" title="self.tooltip">{self.render_label()}</a>')
