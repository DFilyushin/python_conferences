from django.test import TestCase
from django.utils.safestring import SafeString

from core.forms import YesNoForm


class TestYesNoForm(TestCase):

    def test_is_positiv(self):
        self.assertTrue(YesNoForm({'answer': YesNoForm.POSITIV}).is_positiv())
        self.assertFalse(YesNoForm({'answer': YesNoForm.NEGATIV}).is_positiv())
        self.assertFalse(YesNoForm({'answer': '1'}).is_positiv())
        self.assertFalse(YesNoForm().is_positiv())

    def test_render_positiv(self):
        self.assertIsInstance(YesNoForm().render_positiv(), SafeString)

    def test_render_negativ(self):
        self.assertIsInstance(YesNoForm().render_negativ(), SafeString)
