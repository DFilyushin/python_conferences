from django.contrib.contenttypes.models import ContentType
from django.urls import reverse


SET_RELATED_ELEMS_OFF = {'can_delete_related': False, 'can_add_related': False, 'can_change_related': False}


class ModelMixin(object):
    @classmethod
    def get_ct(cls):
        return ContentType.objects.get_for_model(cls)

    def get_admin_change_url(self):
        return reverse(f'admin:{self._meta.app_label}_{self._meta.model_name}_change', args=(self.pk,))

    @classmethod
    def get_admin_add_url(cls):
        return reverse(f'admin:{cls._meta.app_label}_{cls._meta.model_name}_add')


class AddChangeDeleteOffMixin(object):

    def __init__(self, *args, **kw):
        super(AddChangeDeleteOffMixin, self).__init__(*args, **kw)
        for field in self.fields.values():
            vars(field.widget).update(SET_RELATED_ELEMS_OFF)
