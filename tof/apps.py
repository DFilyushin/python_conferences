# -*- coding: utf-8 -*-
import sys

from django.apps import AppConfig
from django.db import connection
from django.utils.translation.trans_real import DjangoTranslation


class TofConfig(AppConfig):
    """Класс настроек приложения.

    Тут будем при старте сервера кэшировать список переводимых полей

    Attributes:
        name: Имя приложения
    """
    name = 'tof'

    def ready(self):
        from django.contrib.contenttypes.models import ContentType

        # Exception if did not make migration
        if connection.introspection.table_names():
            for arg in ('migrate', 'makemigrations'):
                if arg in sys.argv:
                    return  # pragma: no cover
            _, __ = self.models_module.TranslatableField.objects.get_or_create(content_type=ContentType.objects.get_for_model(self.models_module.StaticMessageTranslation),
                                                                               name='translation', title='Translation')
            for field in self.models_module.TranslatableField.objects.all():
                field.add_translation_to_class()
            DjangoTranslation.gettext = self.models_module.StaticMessageTranslation.get_text(DjangoTranslation.gettext)
