
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):

    class Meta:
        permissions = ('is_stock_manager', 'Can log in in stock admin panel'),