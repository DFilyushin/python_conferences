from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import AbstractModel


class HelpArticle(AbstractModel):
    class Meta:
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')

    url = models.CharField(verbose_name=_('Url'), max_length=100, help_text=_('The page url'))
    parent = models.ForeignKey('self', verbose_name=_('Parent'), related_name='children', related_query_name='child', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name or ''

