# Create your admin actions here.
from json import loads
from django.contrib import messages
from django.contrib.admin.helpers import ACTION_CHECKBOX_NAME

from django.utils.decorators import classonlymethod
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

class ActionView(TemplateView):
    short_description = _('My super action on DjGCBV')

    def post(self, *args, **kwargs):
        for obj in self.queryset:
            """Do something with obj."""

    @classonlymethod
    def as_view(cls, **initkwargs):
        response = super(ActionView, cls).as_view(**initkwargs)
        response.short_description = cls.short_description
        return response

    def dispatch(self, admin, request, queryset, *args, **kwargs):
        vars(self).update(admin=admin, model=admin.model, queryset=queryset, request=request)
        return super().dispatch(request, *args, **kwargs)
