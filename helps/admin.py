from django.utils.translation import ugettext_lazy as _
from .models import HelpArticle
from .actions import ActionView

UNKNOWN = _('unknown')

from django.contrib.admin import ModelAdmin, models, sites

class HelpArticleAdmin(ModelAdmin):
    list_display = 'name', 'author', 'editor', 'crteated', 'edited'

    def get_logs_by(self, obj):
        return models.LogEntry.objects.filter(object_id=obj.pk, content_type__model=self.opts.model_name)

    def author(self, obj=None):
        return obj and getattr(self.get_logs_by(obj).first(), 'user', None) or _('unknown')

    def editor(self, obj=None):
        return obj and getattr(self.get_logs_by(obj).last(),'user', None) or _('unknown')

    def crteated(self, obj=None):
        return obj and getattr(self.get_logs_by(obj).first(),'action_time', None) or _('unknown')

    def edited(self, obj=None):
        return obj and getattr(self.get_logs_by(obj).last(),'action_time', None) or _('unknown')



    actions = ActionView.as_view(),

for site in sites.all_sites:
    site.register(HelpArticle, HelpArticleAdmin)