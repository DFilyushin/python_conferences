from django.contrib.admin.templatetags.admin_list import register as admin_list
from django.contrib.admin.templatetags.admin_list import change_list_object_tools_tag as list_tools
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.templatetags.admin_modify import register as admin_modify, change_form_object_tools_tag as change_tools

def get_context(context):
    context['help_button'] = _('Привет')
    return context

@admin_list.tag(name='change_list_object_tools')
def change_list_object_tools_tag(parser, token):
    """Display the row of change list object tools."""
    response = list_tools(parser, token)
    response.func = get_context
    return response

@admin_modify.tag(name='change_form_object_tools')
def change_form_object_tools_tag(parser, token):
    """Display the row of change form object tools."""
    response = change_tools(parser, token)
    response.func = get_context
    return response