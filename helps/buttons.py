from django.db.models import Q
from django.db.models.functions import Length
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, get_language

from core.buttons import URLButton
from helps.models import HelpArticle


class HelpButton(URLButton):
    model = HelpArticle

    def __init__(self, path, **kwargs):
        self.path = path
        article = self.get_article()
        kwargs["text"] = _('Help') if article else _('New help')
        kwargs["url"] = self.get_url(article)

        super().__init__(**kwargs)

    @staticmethod
    def visible_for_object():
        return True

    def get_article(self):
        path = self.get_path()
        query = Q()
        while path:
            path, sep, end = path.rpartition('/')
            query |= Q(url=f'{path}/')
        return self.model.objects.filter(query).annotate(len=Length('url')).order_by('-len').first()

    def get_url(self, article):
        return f"{article.get_admin_change_url()}" if article else f"{self.model.get_admin_add_url()}?url={self.get_path()}"

    def get_path(self):
        lang = get_language()
        if self.path.startswith(f'/{lang}/'):
            __, sep, self.path = self.path.partition(f'/{lang}')
        return self.path
