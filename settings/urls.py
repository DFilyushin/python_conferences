from django.urls import path
from django.contrib.admin.sites import all_sites
from django.conf.urls.i18n import i18n_patterns

urlpatterns = [path(f'{site.name}/', site.urls) for site in all_sites]

urlpatterns = i18n_patterns(*urlpatterns)

try:
    from .local_urls import urlpatterns as local_patterns
    urlpatterns += local_patterns
except ImportError:
    pass
