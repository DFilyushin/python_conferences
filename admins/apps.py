from django.utils.translation import gettext_lazy as _
from django.contrib.admin.apps import AdminConfig


class AdminsConfig(AdminConfig):
    default_site = 'admins.admin.WpAdminSite'
    verbose_name = _("Стартовый Конфигуратор")
