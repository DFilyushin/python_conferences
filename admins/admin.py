from functools import update_wrapper
from django.contrib.admin.options import ModelAdmin
from django.utils.translation import gettext_lazy as _
from django.urls import path
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.admin.sites import site

from django.contrib.admin.sites import AdminSite as BaseAdmin

class AdminSite(BaseAdmin):

    def admin_view(self, view, cacheable=False):

        def wrapper(func):
            def wrapped(*args,**kwargs):
                instance = getattr(func,'__self__', None)
                if isinstance(instance, ModelAdmin):
                    new_instance = type(instance)(instance.model, instance.admin_site)
                    return func.__func__(new_instance, *args,**kwargs)
                return func(*args,**kwargs)
            return wrapped

        return super().admin_view(wrapper(view), cacheable=cacheable)


    permission = ''

    def has_permission(self, request):
        return super().has_permission(request) and (not self.permission or request.user.has_perm(self.permission))



from django.contrib.admin.sites import site, AdminSite

default_admin = site  # AdminSite(name='admin')
produkt_admin = AdminSite(name='admin-produkt')

class WpStockAdminSite(AdminSite):
    permission = 'users.is_stock_manager'

    def has_permission(self, request):
        return super().has_permission(request) and request.user.has_perm(self.permission)

stock_admin = WpStockAdminSite(name='admin-stock')


class WpAdminSite(AdminSite):

    def get_app_list(self, request):
        """ Return a sorted list of all the installed apps that have been registered in this site."""
        apps = [{'name': 'Search Engines',
                 'models':[
                        {'name': 'Google', 'perms': {'change': True}, 'admin_url': 'https://google.com'},
                        {'name': 'Yandex', 'perms': {'change': True}, 'admin_url': 'https://yandex.ru'},
                    ]}]
        return apps + super().get_app_list(request)

    def index(self, request, **kwargs):
        messages.info(request, _('Приветствую'))
        return super().index(request, **kwargs)

    def get_urls(self, *args, **kwargs):

        def wrap(view, cacheable=False):
            def wrapper(*args, **kwargs):
                return self.admin_view(view, cacheable)(*args, **kwargs)
            wrapper.admin_site = self
            return update_wrapper(wrapper, view)

        urlpatterns = [path('navjson/', wrap(self.admin_navigation_javascript, cacheable=True), name='admin_navigation'),]
        return urlpatterns + super().get_urls(*args, **kwargs)

    def admin_navigation_javascript(self, request, *args, **kwargs):
        return JsonResponse(self.get_app_list(request), safe=False)








